# 6502
## The MOSt know machines with that CPU:
- Commodore C64
- 8bit Atari computers
- Terminator's brain
- Bender's brain  
And the newest:
- Commander X16 - hardware does not release yet.

## Hint
I use uppercase names for files because Commodore BASIC can show the letters when it is uppercase. If you use lowercase in BASIC you will see the PETSCII something but not letters ;D

## Compiling for x16
example:
```bash
acme -f cbm -o CURSOR-JOURNEY.PRG CURSOR-JOURNEY.ASM
 ```

## Run emulator
with loaded our program:

```bash
x16emu -prg CURSOR-JOURNEY.PRG
```